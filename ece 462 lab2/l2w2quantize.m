function [ Tquantized ] = l2w2quantize( T, Q )
%L2W2QUANTIZE Summary of this function goes here
%   Detailed explanation goes here

    [m,n] = size(T);
    Tquantized = zeros(m, n);
    for i = 1:8:m
       for j = 1: 8:n
           Tquantized(i:i+7, j:j+7) = T(i:i+7, j:j+7) ./ Q;
       end
    end
    
    Tquantized = round(Tquantized);
end

