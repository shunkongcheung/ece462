function [ T ] = l2w1DCT8by8(  G , F)
%L2W1DCT8BY8 Summary of this function goes here
%   Detailed explanation goes here
    N = 8;
    Size=4;
    
    T = zeros(N);
    
    % 8 by 8: do 4 times 4 by 4 dct
    for i=1:Size:N
       for j=1:Size:N
            T(i:i+Size-1, j:j+Size-1) = l2w1DCT( G(i:i+3, j:j+3), F);
       end
    end

end

