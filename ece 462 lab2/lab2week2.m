%get the image
rgb_image =imread('baby.ppm');

% Q
 %[Ql, Qc] = l2w2GetQDefault(8);
   Ql =l2w2GetQwithR(1);
   Qc =l2w2GetQwithR(4); 
% %  
% convert to ycc
ycc_image= l1w1rgb2ycc(rgb_image);

% perform dct
dct_image = zeros(size(ycc_image));
[m,n,o] = size(ycc_image);
for i = 1:8:m
    for j=1:8:n
        for k = 1: 3
            dct_image(i:i+7, j:j+7,k)= l2w1DCT(ycc_image(i:i+7, j:j+7,k),l2w1DCTForward(8));                          
        end
    end
end

% quantization
quantize_image = zeros(size(ycc_image));
for k = 1: 3
     Q = Qc;
    if(k == 1)
        Q = Ql;
    end
    quantize_image(:,:,k)= l2w2quantize(dct_image(:,:,k),Q);                          
end

% inverse quantization
dequantize_image = zeros(size(ycc_image));
for k = 1: 3
      Q = Qc;
    if(k == 1)
        Q = Ql;
    end
    dequantize_image(:,:,k)= l2w2inquantize(quantize_image(:,:,k),Q);                           
end

% inverse dct
indct_image = zeros(size(ycc_image));
for i = 1:8:m
    for j=1:8:n
        for k = 1: 3
            indct_image(i:i+7, j:j+7,k)= l2w1DCT(dequantize_image(i:i+7, j:j+7,k),l2w1DCTBackward(8));                          
        end
    end
end

%ycc back to rgb
rgb_reconstructed_image= l1w1ycc2rgb(indct_image);

% calculate psnr
psnr = l1w2PSNR(rgb_image, rgb_reconstructed_image);

imshow(rgb_reconstructed_image);