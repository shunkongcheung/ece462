function B = l2w1DCTBackward(N)
    B = l2w1DCTForward(N);
    B = B.';
end