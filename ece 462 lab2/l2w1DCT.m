function T = l2w1DCT( G, F )
    Ftrans = F.';
    T = F*G*Ftrans;
end

