function [ Thead ] = l2w2inquantize( Tquantized, Q )
%L2W2INQUANTIZE Summary of this function goes here
%   Detailed explanation goes here

     [m,n] = size(Tquantized);
    Thead = zeros(m, n);
    for i = 1:8:m
       for j = 1: 8:n
           Thead(i:i+7, j:j+7) = Tquantized(i:i+7, j:j+7) .* Q;
       end
    end
end

