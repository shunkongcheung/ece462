function [ I ] = l2w1BasisImage( N, u, v )
%L2W1BASISIMAGE Summary of this function goes here
%   Detailed explanation goes here
    I = zeros(N);
    
    firstTerm = pi/N;
    for i=1:N
        for j=1:N
            I(i,j) = cos(firstTerm * ((i-1)+0.5)*u) *  cos(firstTerm * ((j-1)+0.5)*v);
        end
    end
    
    
    I = I./ N;
end

