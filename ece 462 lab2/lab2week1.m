% N = 4;
% A = ones(N);
% 
% % Q1 a
% F = l2w1DCTForward(N);
% T = l2w1DCT(A, F);
% 
% % Q1 b
% B = l2w1DCTBackward(N);
% G = l2w1DCT(T, B);
% 
% % Q1 c
% Fh264 = l2w1H264Forward4by4();
% Th264 = l2w1H264(A, Fh264);
% 
% Bh264 = l2w1H264Backward4by4();
% Gh264 = l2w1H264(Th264, Bh264);

% % Q2
N = 8;
% A = ones(N)*3; 
 A = ones(N)*10;
 for i = 1:2:N
    A(:,i) = 5;
 end


T =  l2w1DCT8by8(A, l2w1DCTForward(4));
T264 = l2w1DCT8by8(A, l2w1H264Forward4by4());

% Q3
orignal_energy = norm(A);
DCT_energy = norm(T);
H264_energy = norm(T264);

%Q 4
G = l2w1DCT8by8(T, l2w1DCTBackward(4));
G264 = l2w1DCT8by8(T264, l2w1H264Backward4by4());


% Q5
%I = l2w1BasisImage(6,0,0);
l2w1GenNBasis(6);
