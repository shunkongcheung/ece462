function MSE = l1w2MSE( P, Q )
%   Detailed explanation goes here
P = double(P);
Q = double(Q);

%difference
diff(:,:,1) = P(:,:,1) - Q(:,:,1);
diff(:,:,2) = P(:,:,2) - Q(:,:,2);
diff(:,:,3) = P(:,:,3) - Q(:,:,3);

%square
square = diff.^2;

%summation
MSE = sum(sum(square(:,:,1))) + sum(sum(square(:,:,2))) + sum(sum(square(:,:,3)));

% divide by n and 3
[xlen, ylen] = size(square(:,:,1));
denom = xlen * ylen * 3;
MSE = MSE / denom;

end

