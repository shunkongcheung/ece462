function SNR = l1w2SNR( P, Q )
%L1W2SNR Summary of this function goes here
%   Detailed explanation goes here

 % calculate nominator
 [xlen, ylen] = size(P(:,:,1));
 pzeros = zeros(xlen, ylen, 3);
 nom = l1w2MSE(P, pzeros);
 
 %calculate mse
 mse = l1w2MSE(P, Q);
 
 %calculate snr
 SNR = 10 * log10( nom / mse);

end

