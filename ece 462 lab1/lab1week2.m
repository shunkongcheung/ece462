% Q1, Q2
P = imread('jbeans.ppm');
figure;imshow(P);title('original image');

Q = imread('jbeans_corrupted_sample.ppm');
figure;imshow(Q);title('corrupted sample image');

MSE = l1w2MSE(P,Q);
PSNR = l1w2PSNR(P, Q);
SNR = l1w2SNR(P, Q);

% Q3 Three Plot
rgbimage = imread('jbeans.ppm');
yccimage = l1w1rgb2ycc(rgbimage);

% get size
[xlen, ylen] = size(yccimage(:,:,1));

% create sqrt variances
vars = [10, 20, 40, 80, 100];

% for storing result
mse_y = zeros(size(vars)); mse_cb = zeros(size(vars)); mse_cr = zeros(size(vars));
psnr_y = zeros(size(vars)); psnr_cb = zeros(size(vars)); psnr_cr = zeros(size(vars));
snr_y = zeros(size(vars)); snr_cb = zeros(size(vars)); snr_cr = zeros(size(vars));

% initalize random generator
randn('state', sum(100*clock));

% for all variacnes
for i = 1:length(vars)
    % generate noice
    noise = randn(xlen) * vars(i);
    
    % generate corrupted y matrix
    ycc_corrupted_y = yccimage;
    ycc_corrupted_y(:,:,1) = ycc_corrupted_y(:,:,1)+noise;
    rgb_corrupted_y = l1w1ycc2rgb(ycc_corrupted_y);
    
    mse_y(i) = l1w2MSE(rgbimage, rgb_corrupted_y);
    psnr_y(i) = l1w2PSNR(rgbimage, rgb_corrupted_y);
    snr_y(i) = l1w2SNR(rgbimage, rgb_corrupted_y);
    
    % generate corrupted y matrix
    ycc_corrupted_cb = yccimage;
    ycc_corrupted_cb(:,:,2) = ycc_corrupted_cb(:,:,2)+noise;
    rgb_corrupted_cb = l1w1ycc2rgb(ycc_corrupted_cb);
    
    mse_cb(i) = l1w2MSE(rgbimage, rgb_corrupted_cb);
    psnr_cb(i) = l1w2PSNR(rgbimage, rgb_corrupted_cb);
    snr_cb(i) = l1w2SNR(rgbimage, rgb_corrupted_cb);
    
    % generate corrupted y matrix
    ycc_corrupted_cr = yccimage;
    ycc_corrupted_cr(:,:,3) = ycc_corrupted_cr(:,:,3)+noise;
    rgb_corrupted_cr = l1w1ycc2rgb(ycc_corrupted_cr);
    
    mse_cr(i) = l1w2MSE(rgbimage, rgb_corrupted_cr);
    psnr_cr(i) = l1w2PSNR(rgbimage, rgb_corrupted_cr);
    snr_cr(i) = l1w2SNR(rgbimage, rgb_corrupted_cr);
end

vars = vars .^ 2;

figure; hold on;
plot(vars, mse_y, 'r');plot(vars, mse_cb, 'g');plot(vars, mse_cr, 'b');
hold off;
title('Mean square error verses variance');
xlabel('variance');ylabel('mean square error');
legend('corrupted y','corrupted cb', 'corrupted cr');

figure; hold on;
plot(vars, psnr_y, 'r');plot(vars, psnr_cb, 'g');plot(vars, psnr_cr, 'b');
hold off;
title('Peak Signal-to-noise Ratio verses variance');
xlabel('variance');ylabel('Peak Signal-to-noise Ratio');
legend('corrupted y','corrupted cb', 'corrupted cr');

figure; hold on;
plot(vars, snr_y, 'r');plot(vars, snr_cb, 'g');plot(vars, snr_cr, 'b');
hold off;
title('Signal-to-noise ratio verses variance');
xlabel('variance');ylabel('Signal-to-noise ratio');
legend('corrupted y','corrupted cb', 'corrupted cr');

%Q3 Reconstrut at variance = 6400
noise_6400 = randn(xlen) * sqrt(6400);
ycc_corrupted_y_6400 = yccimage;ycc_corrupted_y_6400(:,:,1) = ycc_corrupted_y_6400(:,:,1)+noise_6400;
rgb_corrupted_y_6400 = l1w1ycc2rgb(ycc_corrupted_y_6400);

ycc_corrupted_cb_6400 = yccimage;ycc_corrupted_cb_6400(:,:,2) = ycc_corrupted_cb_6400(:,:,2)+noise_6400;
rgb_corrupted_cb_6400 = l1w1ycc2rgb(ycc_corrupted_cb_6400);

ycc_corrupted_cr_6400 = yccimage;ycc_corrupted_cr_6400(:,:,3) = ycc_corrupted_cr_6400(:,:,3)+noise_6400;
rgb_corrupted_cr_6400 = l1w1ycc2rgb(ycc_corrupted_cr_6400);

figure; imshow(rgb_corrupted_y_6400);title('Corrupted y with 6400 variance');
figure; imshow(rgb_corrupted_cb_6400);title('Corrupted cb with 6400 variance');
figure; imshow(rgb_corrupted_cr_6400);title('Corrupted cr with 6400 variance');
 
 