function result = l4w2_reconstruct_iframe(Target,QP)
%UNTITLED8 Summary of this function goes here
%   Detailed explanation goes here
Qs = load('Q_matrices.mat');
[m,n] = size(Target);

result = zeros(m,n);

% for each macroblock (I-encoding)
for mi=1:m/16
    for mj=1:n/16
       
        % divide macroblock into four subblock
        tempresult = zeros(16);
        MacroBlock = double(Target((mi-1)*16+1: mi*16, (mj-1)*16+1: mj*16));
        for si=1:2
            for sj=1:2
                curblock = MacroBlock((si-1)*8+1: si*8, (sj-1)*8+1: sj*8);
                Forward = l4w2_forward_dct_quant(curblock, QP, Qs.Q_intra);
                tempresult((si-1)*8+1: si*8, (sj-1)*8+1: sj*8)= l4w2_inverse_dct_quant(Forward, QP, Qs.Q_intra);
            end
        end
        
        % finish one macroblock
        result((mi-1)*16+1: mi*16, (mj-1)*16+1: mj*16) = tempresult;
        
    end
end
result = uint8(result);
end

