% setting information
QP = 2;
Sequence = load('sequence_foreman.mat');
Sequence = Sequence.sequence;

outsequence = zeros(size(Sequence));

% [m,n,length] = size(Sequence);
% for i = 1:2: length
%     outsequence(:,:,i) = l4w2_reconstruct_iframe(Sequence(:,:,i), QP);
%     outsequence(:,:,i+1) = l4w2_reconstruct_pframe(outsequence(:,:,i),Sequence(:,:,i+1), QP);
%  end

outsequence(:,:,1) = l4w2_reconstruct_iframe(Sequence(:,:,1), QP);
[m,n,length] = size(Sequence);
for i = 2: length
    outsequence(:,:,i) = l4w2_reconstruct_pframe(outsequence(:,:,i-1),Sequence(:,:,i), QP);
end

mypsnr = l1w2PSNR(double(outsequence(:,:,length)), double(Sequence(:,:,length)))

play_frames(outsequence, 5);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Target = Sequence(:,:,1);
% predict =l4w2_reconstruct_iframe(Target, QP);
% 
% mymse = immse(Target, uint8(predict));
% figure;
% subplot(1,2,1);imshow(Target);
% subplot(1,2,2);imshow(predict);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Reference = Sequence(:,:,1);
% Target = Sequence(:,:,2);
% predict = l4w2_reconstruct_pframe(Reference, Target, QP);
% figure;
% subplot(1,2,1);imshow(Target);
% subplot(1,2,2);imshow(uint8(predict));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%