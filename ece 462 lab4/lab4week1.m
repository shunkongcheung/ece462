Target = imread('lab4_wk1_tgt.pgm');
Reference = imread('lab4_wk1_ref.pgm');

% figure;subplot(1,2,1);imshow(Reference);
% subplot(1,2,2);imshow(Target);

type ='log';
[prediction, motionVector, flag] = l4w1motionSearch(Reference, Target, type);
figure; imshow(uint8(prediction));

show_mv(motionVector);

mymse = immse(Target,uint8(prediction));

residual = Target - prediction;
figure; imshow(residual);