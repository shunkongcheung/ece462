function F = l2w1DCTForward  (N)
    
    % create matrix NxN matrix
    F = zeros(N);
    
    % loop row
    for i = 1:N

        % create nominator
        nominator = 2;
        if(i ==1)
            nominator = 1;
        end

        % first term
        first = sqrt(nominator/N);

        % frequency
        freq = (i-1) * pi / (2*N);

        % loop column
        for j = 1:N
            F(i,j) = first * cos((2*(j-1)+1) * freq);
        end
        
    end
end