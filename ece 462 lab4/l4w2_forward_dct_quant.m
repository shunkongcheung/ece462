function Output = l4w2_forward_dct_quant(Input,QP, Q_matrix)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
%Forward = l2w1DCT8by8(Input, l2w1DCTForward(4));
Forward = l2w1DCT(Input, l2w1DCTForward(8));
%Forward = dct(Input);
denominator = double(QP) .* Q_matrix;
Output = Forward ./ denominator;
Output = round(Output);

end

