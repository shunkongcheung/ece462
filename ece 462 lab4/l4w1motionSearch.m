function [prediction, motionVector, flag] = l4w1motionSearch(Reference, Target,type)
%L4W1 Summary of this function goes here
%   Detailed explanation goes here

Reference = double(Reference);
Target = double(Target);

% constants
blocksize = 16;
p = 8;
[m,n] = size(Reference);
threshold = 2048;

% values to return
motionVector = size(m*n/blocksize^2, 2);
flag = size(m*n, 1);

% for each macroblock
elementindex = 1;
for i=1: blocksize: m
    for j = 1: blocksize: n

        % for each motion, calculate the absolute value
        bestAbs = 100000;
        bestmi = -1;
        bestmj = -1;

        % different scheme
        if(strcmp(type,'seq'))
            
            % find possible mostions
            [arri, arrj] = getPathArray(i, j , m, n, p, type, blocksize);

            for k = 1: size(arri, 2)
               % curabs = sad(Target(arri(k):arri(k)+blocksize-1, arrj(k):arrj(k)+blocksize-1), Reference(i:i+blocksize-1, j:j+blocksize-1));
                curabs = sad(Reference(arri(k):arri(k)+blocksize-1, arrj(k):arrj(k)+blocksize-1), Target(i:i+blocksize-1, j:j+blocksize-1));
                if(curabs < bestAbs)
                    bestAbs = curabs;
                    bestmi = arri(k);
                    bestmj = arrj(k);
                                      
                end
            end
            
        else
            curp = p/2;
            bestmi = i;
            bestmj = j;
            while (curp >= 1)
                
                 % find possible mostions
                [arri, arrj] = getPathArray(bestmi, bestmj , m, n, curp, type, blocksize);

                for k = 1: size(arri, 2)
                   % curabs = sad(Target(arri(k):arri(k)+blocksize-1, arrj(k):arrj(k)+blocksize-1), Reference(i:i+blocksize-1, j:j+blocksize-1));
                   curabs = sad(Reference(arri(k):arri(k)+blocksize-1, arrj(k):arrj(k)+blocksize-1), Target(i:i+blocksize-1, j:j+blocksize-1));
                    if(curabs < bestAbs)
                        bestAbs = curabs;
                        bestmi = arri(k);
                        bestmj = arrj(k);
                    end
                end
                
                curp = curp / 2;
            end
        end
        
%        record this value
        motionVector(elementindex, 1) = bestmi - i;
        motionVector(elementindex, 2) = bestmj - j;
        flag(elementindex) = (bestAbs < threshold);
        elementindex = elementindex + 1;
    end
end


% make prediction
prediction = zeros(m, n);
elementindex = 1;
for i=1: blocksize: m
    for j = 1: blocksize: n
        
    if(flag(elementindex) == 1)
        prediction(i:i+blocksize-1, j:j+blocksize-1)...
                    = Reference( i+motionVector(elementindex,1):i+motionVector(elementindex,1)+blocksize-1, ... 
                    j+motionVector(elementindex,2):j+motionVector(elementindex,2)+blocksize-1);
    end
        
        elementindex = elementindex + 1;
    end
end

prediction = uint8(prediction);
end

