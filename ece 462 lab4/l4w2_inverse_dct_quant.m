function Output = l4w2_inverse_dct_quant(Input,QP, Q_matrix)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
multiplier = double(QP) .* Q_matrix;
Input = Input .* multiplier;

%Output = idct(Input);
Output = l2w1DCT(Input, l2w1DCTBackward(8));
%Output = l2w1DCT8by8(Input, l2w1DCTBackward(4));
end

