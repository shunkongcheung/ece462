function result = l4w2_reconstruct_pframe(Reference, Target, QP)
%UNTITLED9 Summary of this function goes here
%   Detailed explanation goes here
[prediction, motionVector, flag] = l4w1motionSearch(Reference, Target, 'seq');

Qs = load('Q_matrices.mat');
[m,n] = size(Target);

result = zeros(m,n);

mbindex = 1;
for mi=1:m/16
    for mj=1:n/16
    
        % if flag is zero, the prediction is invalid
        % encode the prediction itself with Q_intra
        tempresult = double(Target((mi-1)*16+1: mi*16, (mj-1)*16+1: mj*16));
        encoder = Qs.Q_intra;

        curflag = flag(mbindex);
                
        % if flag is one, the prediction is valid, 
        % encode the prediction-Referene error with Q_inter
        if (curflag == 1)                
%             tempresult = tempresult ...
%                 -  double(Reference( (mi-1)*16+1 +motionVector(mbindex,1):(mi-1)*16+1+motionVector(mbindex,1)+16-1, ... 
%                     (mj-1)*16+1+ motionVector(mbindex,2):(mj-1)*16+1 +motionVector(mbindex,2)+16-1));
            tempresult =  tempresult -  double(prediction(mbindex));
            encoder = Qs.Q_inter;
        end
        
        
         for si=1:2
            for sj=1:2
                % quantization + dct forward
                tempresult((si-1)*8+1: si*8, (sj-1)*8+1: sj*8) = ...
                    l4w2_forward_dct_quant(tempresult((si-1)*8+1: si*8, (sj-1)*8+1: sj*8), QP, encoder);
                
                % dequantization + dct inverse
                tempresult((si-1)*8+1: si*8, (sj-1)*8+1: sj*8) = ...
                    l4w2_inverse_dct_quant(tempresult((si-1)*8+1: si*8, (sj-1)*8+1: sj*8), QP, encoder);
            end
         end
   

        % always need the transmitted macroblock
        % if flag is zero, the prediction is the sent item
        result((mi-1)*16+1: mi*16, (mj-1)*16+1: mj*16) = tempresult; 
        
        % if flag is one, use reference to get the prediction
        if(curflag == 1)
%             result((mi-1)*16+1: mi*16, (mj-1)*16+1: mj*16) =  ...
%                  double(Reference( (mi-1)*16+1 +motionVector(mbindex,1):(mi-1)*16+1+motionVector(mbindex,1)+16-1, ... 
%                     (mj-1)*16+1+ motionVector(mbindex,2):(mj-1)*16+1 +motionVector(mbindex,2)+16-1)) - ...
%                 result((mi-1)*16+1: mi*16, (mj-1)*16+1: mj*16);

            result((mi-1)*16+1: mi*16, (mj-1)*16+1: mj*16) = double(prediction(mbindex)) + result((mi-1)*16+1: mi*16, (mj-1)*16+1: mj*16);
        end

        % proceed to next macroblock
        mbindex = mbindex + 1;
    end
end
result = uint8(result);
end

