function [arri, arrj] = getPathArray(posi, posj ,sizem, sizen, p, type, blocksize)
%UNTITLED Summary of this function goes here
% (i, j): current position
% [m, n]: image size
% p: search pixel positions

    % there are p^2 element. each element has a position
    arri = [];
    arrj = [];

    if(type == 'log')
        
        % treat p as an offset in this case
        for i =-1:1:1
            for j = -1: 1: 1
                ti = posi + i * p;
                tj = posj + j * p;
                
                % within range
                if( (ti>0 && tj > 0) && (ti+blocksize-1 <= sizem && tj+blocksize-1 <=sizen))
                    arri = [arri, ti];
                    arrj = [arrj, tj];
                end
            end
        end
        
        
    elseif(type == 'seq')
        
        % loop through all possible elements
        for i = posi-p: posi+p
            for j = posj-p:posj+p
                
                % within range
                if( (i>0 && j > 0) && (i+blocksize-1 <= sizem && j+blocksize-1 <=sizen))
                    arri = [arri, i];
                    arrj = [arrj, j];
                end
            end
        end
    end

end

