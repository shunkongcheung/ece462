function [outputArg1] = ReverseLL_Haar2D(signal,L, M, N)
%UNTITLED8 Summary of this function goes here
%   Detailed explanation goes here
    outputArg1 = signal;
    M = M/ (2^(L-1));
    N = N/ (2^(L-1));
    for i =1:L
     %   outputArg1(1:M/l, 1:N/l) = ReverseL1_Haar2D(outputArg1(1:M/l, 1:N/l),M/l, N/l);
        outputArg1(1:M, 1:N) = ReverseL1_Haar2D(outputArg1(1:M, 1:N), M, N);
        M = M*2;
        N= N*2;

    end
end

