function [outputArg1] = rev_cdf2d(signal,M, N)
%REV_CDF2D Summary of this function goes here
%   Detailed explanation goes here
    outputArg1 = signal;
    for i =1: M
        outputArg1(i,:) = rev_cdf(outputArg1(i,:));
    end
    for i =1: N
        outputArg1(:,i) = rev_cdf(outputArg1(:,i));
    end
end

