function [outputArg1] = ReverseL1_Haar2D(signal, M, N)
%UNTITLED7 Summary of this function goes here
%   Detailed explanation goes here

    outputArg1 = signal;
    for i =1: M
        outputArg1(i,:) = ReverseL1_Haar1D(outputArg1(i,:), N);
    end
    for i =1: N
        outputArg1(:,i) = ReverseL1_Haar1D(outputArg1(:,i), M);
    end
    
%     lowpass = [1/sqrt(2), 1/sqrt(2) ];
%     highpass = [1/sqrt(2), -1/sqrt(2) ]; 
% 
%     LL1 = lowpass .* transpose(lowpass);
%     HL1 = lowpass .* transpose(highpass);
%     LH1 = highpass .* transpose(lowpass);
%     HH1 = highpass .* transpose(highpass);
%     
%     h00 = zeros([M/2, N/2]); 
%     h01 = zeros([M/2, N/2]); 
%     h10 = zeros([M/2, N/2]); 
%     h11 = zeros([M/2, N/2]); 
%     
%      for i = 1:M/2
%         for j = 1:N/2
%             h00(i*2-1, j*2-1) =signal(i, j) * LL1(1,1);
%             h00(i*2-1, j*2) =signal(i, j) * LL1(1,2);
%             h00(i*2, j*2-1) =signal(i, j) * LL1(2,1);
%             h00(i*2, j*2) =signal(i, j) * LL1(2,2);
%         end
%      end
%      
%      for i = 1:M/2
%         for j = N/2+1:N
%             h01(i*2-1, (j - N/2)*2-1) =signal(i, j) * LH1(1,1);
%             h01(i*2-1, (j - N/2)*2) =signal(i, j) * LH1(1,2);
%             h01(i*2, (j - N/2)*2-1) =signal(i, j) * LH1(2,1);
%             h01(i*2, (j - N/2)*2) =signal(i, j) * LH1(2,2);
%         end
%      end
%     
%      for i = M/2+1:M
%         for j = 1:N/2
%             h10((i - M/2)*2-1, j*2-1) =signal(i, j) * HL1(1,1);
%             h10((i - M/2)*2-1, j*2) =signal(i, j) * HL1(1,2);
%             h10((i - M/2)*2, j*2-1) =signal(i, j) * HL1(2,1);
%             h10((i - M/2)*2, j*2) =signal(i, j) * HL1(2,2);
%         end
%      end
%     
%      for i = M/2+1:M
%         for j = N/2+1:N
%             h11((i - M/2)*2-1, (j - N/2)*2-1) =signal(i, j) * HH1(1,1);
%             h11((i - M/2)*2-1, (j - N/2)*2) =signal(i, j) * HH1(1,2);
%             h11((i - M/2)*2, (j - N/2)*2-1) =signal(i, j) * HH1(2,1);
%             h11((i - M/2)*2, (j - N/2)*2) =signal(i, j) * HH1(2,2);
%         end
%      end
%      
%      outputArg1 = h00 + h11 + h10 + h01;
end

