function [reconstruct] = bit_plane_encode(image,level)
%BIT_PLANE_ENCODE Summary of this function goes here
%   Detailed explanation goes here
% 
% 
% % find sign plane and absolute values
 sign_plane = sign(image);
 abs_mat = abs(image);

% the reconstructed image
reconstruct = zeros(size(image));

start = floor(log2(max(max(image))));

for i = start: -1 : (start - level)
    % get current level image
    curimg = abs_mat ./ 2^(i);
  
    % find floor value from absolute value
    curimg = floor(curimg);

    % get the mods, this is the image that is sending
    mods = mod(curimg, 2);  
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % in between, we are sending the mods
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    reconstruct = floor(reconstruct ./ 2^i);
    reconstruct = reconstruct .* 2^i;
    
    reconstruct = reconstruct + mods.* 2^i;

    % get most significant bit of all coefficients
    msbs = log2(reconstruct);
    
    greater = (msbs> i);
    equal = (floor(msbs) == i);

    % put up current values
    reconstruct = reconstruct + equal * 2^(i-1); 
    reconstruct = reconstruct + ((greater .* mods + greater .* (mods-1)) * 2^(i-1));
    
end

reconstruct = sign_plane .* reconstruct;

end

