function [outputArg1] = ForwardLL_Haar2D(signal,L, M, N)
%UNTITLED6 Summary of this function goes here
%   Detailed explanation goes here

    outputArg1 = signal;
    for l = 1: L
      %   outputArg1(1:M/l, 1:N/l) = ForwardL1_Haar2D(outputArg1(1:M/l, 1:N/l), M/l, N/l);
        outputArg1(1:M, 1:N) = ForwardL1_Haar2D(outputArg1(1:M, 1:N), M, N);
        M = M/2;
        N= N/2;
    end
end

