 signal1D = [1,2,3,4,5,6,7,8, 1,2,3,4,5,6,7,8];
 N = 16;
% output1D = ForwardL1_Haar1D(signal1D, N); 
% origin1D = ReverseL1_Haar1D(output1D, N);

 signal2D = signal1D .* transpose (signal1D);
% output2D = ForwardL1_Haar2D(signal2D, N, N);
% origin2D = ReverseL1_Haar2D(output2D, N, N);
% 
% 
% out1 = ForwardLL_Haar2D(signal2D, 1, N, N);
 out2 = ForwardLL_Haar2D(signal2D, 3, N, N);
 org2 = ReverseLL_Haar2D(out2, 3, N, N);

img = imread ('barbara.ppm');
imgy = img(:,:,1);

[m,n] = size(imgy);
imgy = double(imgy);
% outy = ForwardLL_Haar2D(imgy, 3, m,n);
 outy = fwd_cdf2d_l(imgy, 3, m, n);

 %disp_dwt(outy, 3);

%orgy = ReverseLL_Haar2D(outy, 3, m,n);
orgy = rev_cdf2d_l(outy, 3, m, n);
imagesc(orgy);