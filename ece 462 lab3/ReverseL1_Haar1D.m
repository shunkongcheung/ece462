function [outputArg1] = ReverseL1_Haar1D(signal,N)
%REVERSEL1_HAAR1D Summary of this function goes here
%   Detailed explanation goes here
    lowpass = [1/sqrt(2), 1/sqrt(2) ];
    highpass = [1/sqrt(2), -1/sqrt(2) ];  
    
    low = zeros([N,0]);
    high = zeros([N, 0]);
    
    for i = 1: N/2
        low(i*2-1) = signal(i) * lowpass(1);
        low(i*2) = signal(i)*lowpass(2);
    end
    
    for i = N/2+1: N
        high((i - N/2)*2-1) = signal(i) * highpass(1);
        high((i - N/2)*2) = signal(i)*highpass(2); 
    end
    
    outputArg1 = low + high;
end

