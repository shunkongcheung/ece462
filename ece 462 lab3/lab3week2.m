% % part 1
% test = zeros(64);
% 
% locs = [64*3/4, 64*3/4;
%        64/4, 64*3/4;
%        64*3/4, 64/4;
%       
%        64*3/8,64/8;
%        64/8,64*3/8;
%        64*3/8,64*3/8;
%        
%        64*3/16,64/16;
%        64/16,64*3/16;
%        64*3/16,64*3/16;
%        
%        64/16,64/16;
%        ];
% 
% test = double(test);
% 
% for i = 1 : 10
%     test(locs(i, 1), locs(i, 2)) = 1;
%     %out = rev_cdf2d_l(test, 3, 64,64);
%     out = ReverseLL_Haar2D(test, 3, 64,64);
%     subplot(4, 3,i); imshow(out);
%     test(locs(i, 1), locs(i, 2)) = 0;
% end


% part 2
 img = imread('uc_quad.pgm');
 
 [m,n] = size(img);
% 
% % get forwarded image
img = double(img);
% image = fwd_cdf2d_l(img, 5, m,n);
 image = ForwardLL_Haar2D(img, 5, m, n);

% 
psnrs = zeros(6, 1);

for level = 1: 6
    reconstruct= bit_plane_encode(image, level-1);

    transformed = ReverseLL_Haar2D(reconstruct, 5, m, n);
  %  transformed = rev_cdf2d_l(reconstruct, 5, m,n);
    
    subplot(2, 3, level); imshow(uint8(transformed));

    % psnr
    image_int = uint8(img);
    transformed_int = uint8(transformed);
    psnrs(level, 1) = psnr(transformed_int, image_int);
end


