function [outputArg1] = ForwardL1_Haar2D(signal,M, N)
%UNTITLED5 Summary of this function goes here
%   Detailed explanation goes here
    outputArg1 = signal;
    
    for i=1:M
        outputArg1(i,:) = ForwardL1_Haar1D(outputArg1(i,:) , N);        
    end
    for i=1:N
        outputArg1(:,i) = ForwardL1_Haar1D(outputArg1(:,i), M);
    end

% 
%       lowpass = [1/sqrt(2), 1/sqrt(2) ];
%     highpass = [1/sqrt(2), -1/sqrt(2) ]; 
% 
%     LL1 = lowpass .* transpose(lowpass);
%     HL1 = lowpass .* transpose(highpass);
%     LH1 = highpass .* transpose(lowpass);
%     HH1 = highpass .* transpose(highpass);
%     
%     h00 = zeros([M/2, N/2]); 
%     h01 = zeros([M/2, N/2]);
%     h10 = zeros([M/2, N/2]);
%     h11 = zeros([M/2, N/2]);
%     
%     
%     for i = 1:M/2
%         for j = 1:N/2
%             h00(i, j) =sum( sum(signal(i*2-1: i*2, j*2-1 : j*2) .* LL1));
%             h01(i, j) =sum( sum(signal(i*2-1: i*2, j*2-1 : j*2) .* LH1));
%             h10(i, j) =sum( sum(signal(i*2-1: i*2, j*2-1 : j*2) .* HL1));
%             h11(i, j) =sum( sum(signal(i*2-1: i*2, j*2-1 : j*2) .* HH1));
%         end
%     end
%     
%     outputArg1 = [h00, h01; h10, h11];
end

