function [result] = ForwardL1_Haar1D(signal,N)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

    lowpass = [1/sqrt(2), 1/sqrt(2) ];
    highpass = [1/sqrt(2), -1/sqrt(2) ];    

    result = zeros([N, 0]);
    
    for i=1:N/2
        result(i) = signal(i*2-1) * lowpass(1) + signal(i*2) * lowpass(2);
        result(i + N/2) = signal(i*2-1) * highpass(1) + signal(i*2) * highpass(2);
    end
end

